<?php include('header.php'); ?>

<!-- header end -->   
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Recent Project</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Recent Project</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
      <div class="row">
        <?php
                $latPhotos=$mysqli->query("SELECT * FROM projects WHERE proid=12");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["proid"];
                $title=$SiPhotos["title"];
                $sub=$SiPhotos["sub_title"];
                $description=$SiPhotos["description"];
                $context=$SiPhotos["context"];
                $themes=$SiPhotos["themes"];
                $major=$SiPhotos["major"];
                $incs=$SiPhotos["include"];
                $hivs=$SiPhotos["hiv"];
                $peers=$SiPhotos["peer"];
                $strs=$SiPhotos["strength"];
                $comms=$SiPhotos["community"];

                
                ?>
        <div class="list sec-title">
          <div class="col-lg-6">
          <h1 class=""> LINKAGES</h1>
            <span class="line"></span>
        </div>
        <div class="col-lg-6 text-right">
          <a href="project.html" class="breadcrumb">
          <i class="fa fa-tasks" aria-hidden="true"></i>
Back to Project
        </a>
        </div>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-12">
       
         <ul class="list sec-title">
          
          <p class="details"> 
             <?=$description?>

          </p>
                  
                  <h4 class="margin10">  OBJECTIVES</h4>
                  <ul class="theme">  
                    <li> <?=$context?> </li>
                  </ul>
                  <h4 class="margin32">APPROACH</h4>
                  <p class="details"> 
                    <?=$themes?>
                  </p>
                   <h1> Major Activities</h1>
          <span class="line"></span>
           <h5 class="Activities"><?=$major?></h5>
 <p class="activities_detail"><?=$incs?></p>
 <h5 class="Activities"><?=$hivs?></h5>
 <p class="activities_detail">This includes institutional behavioral communication through community and peer based outreach education using variety of communication channel and media including individually focused health education and Drop-In-Centers (DIC) operation for positive behavior change and maintenance. The education also includes condom promotion, distribution, referral and follow up for STI diagnosis and treatment. This also includes counseling,testing, care, support, treatment and others.</p>
 <h5 class="Activities"><?=$strs?></h5>
 <p class="activities_detail"><?=$comms?></p>
  </ul>

      </div>
    </div>
  </div><?php }?>
  </div>

  <!--footer -->
 <?php include('footer.php');?>
</body>
</html>