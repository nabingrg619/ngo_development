 
<?php include('header.php'); ?>
<div class="page-top parallax dark-translucent page-top_volunter">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Recent Project</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Recent project</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
 <!-- start volunter -->
   <section class="volunter projects_cause">
    <div class="container">
      <div class="row">
        <?php
                $latPhotos=$mysqli->query("SELECT * FROM projects WHERE proid=7");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["proid"];
                $title=$SiPhotos["title"];
                $sub=$SiPhotos["sub_title"];
                $area=$SiPhotos["area"];
                $donor=$SiPhotos["donor"];
                $dur=$SiPhotos["duration"];
                $image=$SiPhotos["image"];



                
                ?>
       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <figure>
          <div class="img">
            <a href="#">
              <img src="img/<?=$image?>" alt="img">
            </a>
          </div>
            <figcaption>
              <div class="overlay">
               <div class="cause_title">
                <h3 class="text-center">
                <a href="local_right_Project1.php"><?=$title?></a>
                </h3>
                <p class="text-center"> <?=$sub?>  </p>
              </div>
                <div class="cause_detail recent_cause">
                  <ul>  
                    <li> <span>Donor : </span><?=$donor?></li>
                     <li> <span>Working areas : </span><?=$area?></li>
                      <li> <span>Duration : </span><?=$dur?></li>
                  </ul>
                </div><?php }?>
              </div>

                <div class="text-center">
                <a href="local_right_Project1.php" class="btn btn-donate">Project Details</a>
              </div>
            </figcaption>
        </figure>
       </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <figure>
          <div class="img">
             <?php
                $latPhotos=$mysqli->query("SELECT * FROM projects WHERE proid=12");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["proid"];
                $title=$SiPhotos["title"];
                $sub=$SiPhotos["sub_title"];
                $area=$SiPhotos["area"];
                $donor=$SiPhotos["donor"];
                $dur=$SiPhotos["duration"];
                $image=$SiPhotos["image"];


                
                ?>
            <a href="#">
              <img src="img/<?=$image?>" alt="img">
            </a>
          </div>
            <figcaption>
              <div class="overlay">
               <div class="cause_title">
                <h3 class="text-center">
                <a href="linkages.php"><?=$title?></a>
                </h3>
                <p class="text-center"> <?=$sub?> </p>
              </div>
                <div class="cause_detail recent_cause">
                  <ul>  
                    <li> <span>Donor : </span> <?=$donor?></li>
                                   <li> <span>Project Area : </span><?=$area?>
</li>
                     <li> <span>Target Groups: </span><?=$dur?></li>
       
                  </ul>
                </div><?php }?>
              </div>

                <div class="text-center">
                <a href="linkages.php" class="btn btn-donate">Project Details</a>
              </div>
            </figcaption>
        </figure>
       </div>
      
</div>

</div>
</section>
<!-- End volunter -->

<!--footer -->
 <?php include('footer.php');?>
 </body>
</html>