
<?php
require_once("inc/config.php");
$successMsg = "";
$scr = "";
$strDate = strtotime(date('Y-m-d H:i:s'));
$ActivityId=$_GET['id'];
$Blogs = $mysqli->query("select * from projects where proid=$ActivityId");
$SiProjects=$Blogs->fetch_array();
$Title=$SiProjects['title'];
$Description=$SiProjects["description"];
$Photo=$SiProjects["image"];
$contexts=$SiProjects["context"];
$themes=$SiProjects["themes"];
$sub=$SiProjects["sub_title"];
$donor=$SiProjects["donor"];
$area=$SiProjects["area"];
$duration=$SiProjects["duration"];
$major=$SiProjects["major"];
$inc=$SiProjects["include"];
$hiv=$SiProjects["hiv"];
$peer=$SiProjects["peer"];
$str=$SiProjects["strength"];
$comm=$SiProjects["community"];
$filename16=$Photo;        

if(isset($_POST['submit'])){
  $DataTitle = ($_POST['txtTitle']);
  $DataDescription = $_POST['txtDescription'];
  $context=$_POST['context'];
  $themess=$_POST['themes'];
  $sub=$_POST['sub'];
  $Donors=$_POST['donor'];
  $Area=$_POST['area'];
  $Dur=$_POST['dur'];
  $majors=$_POST['major'];
  $incs=$SiProjects["include"];
  $hivs=$SiProjects["hiv"];
  $peers=$SiProjects["peer"];
  $strs=$SiProjects["strength"];
  $comms=$SiProjects["community"];
  
  define ("MAX_SIZE","55000");
  function getExtension($str) {
   $i = strrpos($str,".");
   if (!$i) { return ""; }
   $l = strlen($str) - $i;
   $ext = substr($str,$i+1,$l);
   return $ext;
 } 
 $errors=0;
 if($_SERVER["REQUEST_METHOD"] == "POST")
 {
  $image =$_FILES["file"]["name"];
  $uploadedfile = $_FILES['file']['tmp_name'];  
  if ($image) 
  {

    $filename = stripslashes($_FILES['file']['name']);

    $extension = getExtension($filename);
    $extension = strtolower($extension);


    if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
    {

      $change='<div class="msgdiv">Unknown Image extension </div> ';
      $errors=1;
    }
    else
    {

     $size=filesize($_FILES['file']['tmp_name']);


     if ($size > MAX_SIZE*5024)
     {
      $change='<div class="msgdiv">You have exceeded the size limit!</div> ';
      $errors=1;
    }


    if($extension=="jpg" || $extension=="jpeg" )
    {
      $uploadedfile = $_FILES['file']['tmp_name'];
      $src = imagecreatefromjpeg($uploadedfile);

    }
    else if($extension=="png")
    {
      $uploadedfile = $_FILES['file']['tmp_name'];
      $src = imagecreatefrompng($uploadedfile);

    }
    else
    {
      $src = imagecreatefromgif($uploadedfile);
    }

    echo $scr;

    list($width,$height)=getimagesize($uploadedfile);


    $newwidth=120;
    $newheight=94;

    $newwidth1=$width;
    $newheight1=$height;

    $tmp1=imagecreatetruecolor($newwidth1,$newheight1);
    imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);
    $filename1 = "../img/".$strDate.strtolower(($_FILES['file']['name']));
    $filename16 =$strDate.strtolower(($_FILES['file']['name']));
    imagejpeg($tmp1,$filename1,100);
    imagedestroy($src);
    imagedestroy($tmp1);
  }}
}

$add_sql = $mysqli->query("UPDATE projects SET title = '$DataTitle', description = '$DataDescription',image = '$filename16',context='$context',themes='$themess',sub_title='$sub',donor='$Donors',area='$Area',duration='$Dur',major='$majors',include='$incs',hiv='$hivs',peer='$peers',strength='$strs',community='$comms' WHERE proid=$ActivityId");

if($add_sql = TRUE){
   echo "<meta http-equiv='refresh' content='0'>";
   echo "<script>alert('Successfully Activities Updated.');
                 window.location.href='Editprojects.php';
         </script>";
}else{
  $successMsg = '<div class="alert alert-success">Some Error!!! Contact to Web Page Nepal for IT Help.</div>';
}
}
?>
<?php include('inc/head.php');?>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    #top nav, aside menu
    require_once("inc/header.php");
    ?>
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Project
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Project</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <?=$successMsg?>
            <!-- quick email widget -->
            <div class="box box-info">
              <div class="box-header">
                <i class="fa fa-envelope"></i>

                <h3 class="box-title">Edit PROJECT</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                </div>
                <!-- /. tools -->
              </div>
              <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">
                  <label>title</label>
                  <div class="form-group">
                    <input type="text" class="form-control" value="<?=$Title?>" name="txtTitle" placeholder="Title">
                  </div>
                  <label>sub-title</label>
                  <div class="form-group">
                    <input type="text" class="form-control" value="<?=$sub?>" name="sub" placeholder="sub-Title">
                  </div>
                  <label>donor</label>
                  <div>
                    <textarea name="donor" class="textarea" placeholder="<?=$donor?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>area</label>
                  <div>
                    <textarea name="area" class="textarea" placeholder="<?=$area?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                   <label>duration</label>
                  <div>
                    <textarea name="dur" class="textarea" placeholder="<?=$duration?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>description</label>
                  <div>
                    <textarea name="txtDescription" class="textarea" placeholder="<?=$Description?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>context</label>
                  <div>
                    <textarea name="context" class="textarea" placeholder="<?=$contexts?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>themes</label>
                  <div>
                    <textarea name="themes" class="textarea" placeholder="<?=$themes?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>Major activites</label>
                  <div>
                    <textarea name="major" class="textarea" placeholder="<?=$major?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>inc</label>
                  <div>
                    <textarea name="include" class="textarea" placeholder="<?=$inc?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>hiv</label>
                  <div>
                    <textarea name="hiv" class="textarea" placeholder="<?=$hiv?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>peer</label>
                  <div>
                    <textarea name="peer" class="textarea" placeholder="<?=$peer?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>str</label>
                  <div>
                    <textarea name="strength" class="textarea" placeholder="<?=$str?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <label>community</label>
                  <div>
                    <textarea name="community" class="textarea" placeholder="<?=$comm?>" 
                    style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  
                 
                  <label>image</label>
                  <div class="form-group">
                    <label class="form-label">Upload Photo</label>
                    <input name="file" type="file" />
                  </div>
                  
                  <div class="box-footer clearfix">
                    <button type="submit" name="submit" class="pull-right btn btn-default" id="sendEmail">Save
                      <i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                  </form>
                </div>
              </div>

            </section>
            <!-- /.Left col -->

          </div>
          <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
      </div>
   <?php include('inc/foot.php');?>