 <?php include('header.php');?> 
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>News and Events</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">News</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
    <div class="row">
      
      <div class="col-lg-9">
        <?php
                $latPhotos=$mysqli->query("SELECT * FROM activites WHERE acid=5");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["acid"];
                $title=$SiPhotos["TITLE"];
                $description=$SiPhotos["Description"];
                $image=$SiPhotos["image"];
                $date=$SiPhotos["dates"];
                ?>
        <div class="news">

          <div class="news_img">
            <img src="img/<?=$image?>">
          </div>
        
          <div class="news_detail">
             <h5><a href="linkages.php"><?=$title?></a></h5>
            <span><i class="fa fa-clock-o span" aria-hidden="true"></i><?=$date?></span>
            <p> <?=$description?></p>
            <div class="read_btn">
              <a href="news_detailpage.php">Read More</a>
            </div>
          </div><?php }?>
        </div>
        <div class="news">
           <?php
                $latPhotos=$mysqli->query("SELECT * FROM activites WHERE acid=6");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["acid"];
                $title=$SiPhotos["TITLE"];
                $description=$SiPhotos["Description"];
                $image=$SiPhotos["image"];
                $date=$SiPhotos["dates"];
                ?>
          <div class="news_img">

            <img src="img/<?=$image?>">
          </div>
          <div class="news_detail">
             <h5><a href="news_detailpage.php"><?=$title?></a></h5>
            <span><i class="fa fa-clock-o span" aria-hidden="true"></i><?=$date?></span>
            <p><?=$description?> </p>
             <div class="read_btn">
              <a href="news_detailpage.php">Read More</a>
            </div>
          </div><?php }?>
        </div>
        <div class="news">
          <?php
                $latPhotos=$mysqli->query("SELECT * FROM activites WHERE acid=7");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["acid"];
                $title=$SiPhotos["TITLE"];
                $description=$SiPhotos["Description"];
                $image=$SiPhotos["image"];
                $date=$SiPhotos["dates"];
                ?>
          <div class="news_img">
            <img src="img/<?=$image?>">
          </div>
          <div class="news_detail">
            <h5><a href="news_detailpage2.php"><?=$title?></a></h5>
            <span><i class="fa fa-clock-o span" aria-hidden="true"><?=$date?></i></span>
            <p><?=$description?></p>
             <div class="read_btn">
              <a href="news_detailpage2.php">Read More</a>
            </div>
          </div>
        </div><?php }?>
      </div>
      <div class="col-lg-3 sidebar">
         <div class="block">
    <h4>News & Events</h4>
    <div class="newslist">
      <ul>
        <li><a href="news_detailpage.php?"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
        <li><a href="news_detailpage.php"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
        <li><a href="news_detailpage.php"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
      </ul>
    </div>
  </div>
      </div>
    </div>
  </div>
  </div>

 <?php include('footer.php');?> 