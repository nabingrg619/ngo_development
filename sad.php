<?php 
require_once("AdminLTE/inc/config.php");
?>
<!-- header  --> 
<?php include('header.php'); ?> 
 <!-- banner start --> 
  <!-- ================ -->
  <div class="banner"> 
    <!-- slideshow start --> 
    <!-- ================ -->
    <div class="slideshow"> 
      <!-- slider revolution start --> 
      <!-- ================ -->
      <div class="slider-banner-container">
        <div class="slider-banner">
          <?php
                $latPhotos=$mysqli->query("SELECT * FROM slideshow");
                while($SiPhotos=$latPhotos->fetch_array()){
                $id=$SiPhotos["id"];
                $title=$SiPhotos["image"];
                ?>
          <ul>
            <!-- slide 1 start -->
            <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Canyoning Pokhara"> 
              <!-- main image --> 
              <img src="img/<?=$title?>"  alt="slidebg1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                            
            </li>
            
            
          </ul>
          <?php }?>
          <div class="tp-bannertimer tp-bottom"></div>
        </div>
      </div>
      <!-- slider revolution end --> 
    </div>
    <!-- slideshow end -->
    <div class="booking-form">
      <div class="book-title">
        <h2>Donate Now</h2>
        <p>Be a part of the world by make donation</p>
      </div>
      <form method="post" action="booking_form.php" class="book-form">
    <div class="form-group"> <i class="a fa-user"></i>
          <input type="text" name="txtBookedName" class="form-control" placeholder=" Name...">
        </div>
       <div class="form-group"> <i class="fa fa-envelope"></i>
          <input type="text" name="txtBookedEmail" class="form-control" placeholder=" Email...">
        </div>
        <div class="form-group"> <i class="fa fa-map-marker"></i>
          <input type="text" name="txtBookedArrivalDate" class="form-control" placeholder=" City...">
        </div>
        
        <div class="form-group">
           <input type="text" name="amount" class="form-control" placeholder="Amount...">
        </div>
        
        <button type="submit" class="btn btn-warning">Donate</button>
      </form>
    </div>
  </div>
  <!-- banner end -->
   <!--ABOUT  -->
  <div class="AboutUs">
    <div class="container-fluid">
      <div class="title text-center">
        <h2>Child and Women Empowerment</h2>
        <div class="seperator"><span class="square-box"></span></div>
      </div>
      <div class="row">
            <div class="col-lg-6 pad0">
              <?php
                $latPhotos=$mysqli->query("SELECT * FROM about WHERE Aboutid=3");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["Aboutid"];
                $title=$SiPhotos["Title"];
                $description=$SiPhotos["description"];
                $image=$SiPhotos["image"];
                ?>
              <div class="about_img">
                <img src="img/<?=$image?>">
              </div>
            </div>
            <div class="col-lg-6 pad0">
             <div class="about_bgimg">
               <div class="about_info">
               
                <h3><?=$title?></h3>
                <p>
                  <?=$description?>

                </p>
                <a href="<?=$description?>" class="btn btn-outline">Read More</a>
                
               </div>
               <?php }?>
             </div>
            </div>
      </div>
    </div>
  </div>
  <!--ABOUT  -->
    <!--Causes  -->
  <div class="Causes">
    <div class="container-fluid">
      <div class="title text-center">
        <h2>Our Works</h2>
        <div class="seperator"><span class="square-box"></span></div>
        
      </div>
      <div class="row">
        <?php
                $latPhotos=$mysqli->query("SELECT * FROM our_work WHERE workid=14");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $workid=$SiPhotos["workid"];
                $title=$SiPhotos["Title"];
                $description=$SiPhotos["description"];
                $donation=$SiPhotos["donation"];
                $Sponser=$SiPhotos["sponsers"];
                 $date=$SiPhotos["date"];
                $image=$SiPhotos["photo"];
                ?>
        <div class="col-lg-3" >
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/<?=$image?>">
              </div>
              <div class="cause_title">
                <h3 class="text-center"> <?=$title?></h3>
                <p class="text-center"> <?=$description?> </p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Donation : </span><?=$donation?></li>
                     <li> <span>Sponsers : </span><?=$Sponser?></li>
                      <li> <span>Duration : </span><?=$date?></li>
                  </ul>
                </div>
              </div>
          </div>
        <?php }?>
        </div>
        <div class="col-lg-3" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
          <?php
                $latPhotos=$mysqli->query("SELECT * FROM our_work WHERE workid=15");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $workid=$SiPhotos["workid"];
                $title=$SiPhotos["Title"];
                $description=$SiPhotos["description"];
                $donation=$SiPhotos["donation"];
                $Sponser=$SiPhotos["sponsers"];
                 $date=$SiPhotos["date"];
                $image=$SiPhotos["photo"];
                ?>
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/<?=$image?>">
              </div>
              <div class="cause_title">
                <h3 class="text-center"> <?=$title?></h3>
                <p class="text-center"> <?=$description?></p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Donation :</span><?=$donation?></li>
                     <li> <span>Sponsers : </span><?=$Sponser?></li>
                      <li> <span>Duration :</span><?=$date?></li>
                  </ul>
                </div>
              </div>
          </div>
          <?php }?>
        </div>
        <div class="col-lg-3" data-animation-effect="fadeInDownSmall" data-effect-delay="200">
          <?php
                $latPhotos=$mysqli->query("SELECT * FROM our_work WHERE workid=16");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $workid=$SiPhotos["workid"];
                $title=$SiPhotos["Title"];
                $description=$SiPhotos["description"];
                $donation=$SiPhotos["donation"];
                $Sponser=$SiPhotos["sponsers"];
                 $date=$SiPhotos["date"];
                $image=$SiPhotos["photo"];
                ?>
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/cause3.jpg">
              </div>
              <div class="cause_title">
                <h3 class="text-center"><?=$title?> </h3>
                <p class="text-center">  <?=$description?></p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Donation :</span><?=$donation?></li>
                     <li> <span>Sponsers : </span><?=$Sponser?></li>
                      <li> <span>Duration :</span><?=$date?></li>
                  </ul>
                </div>
              </div>
          </div><?php }?>
        </div>
       <div class="col-lg-3" data-animation-effect="fadeInDownSmall" data-effect-delay="300">
        <?php
                $latPhotos=$mysqli->query("SELECT * FROM our_work WHERE workid=17");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $workid=$SiPhotos["workid"];
                $title=$SiPhotos["Title"];
                $description=$SiPhotos["description"];
                $donation=$SiPhotos["donation"];
                $Sponser=$SiPhotos["sponsers"];
                 $date=$SiPhotos["date"];
                $image=$SiPhotos["photo"];
                ?>
          <div class="main_cause">
              <div class="img_cause">
                <img src="img/<?=$image?>">
              </div>
              <div class="cause_title">
                <h3 class="text-center"><?=$title?></h3>
                <p class="text-center"><?=$description?></p>
              </div>
              <div class="cause_bg">
                <div class="cause_detail">
                  <ul>  
                    <li> <span>Donation:</span><?=$donation?></li>
                     <li> <span>Sponsers : </span><?=$Sponser?></li>
                      <li> <span>Duration :</span><?=$date?></li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div><?php }?>
  </div>
  <!--Causes-->

  <!--Testimonial  -->
    <div class="section-content parallax-bg ">
      <div class="testimonial-wrapper">
          <div class="title text-center testi">
        <h2>Donor Speak</h2>
        <div class="seperator sep_testi"><span class="square-box"></span></div>
        
      </div>
        <div class="owl-carousel owl-theme">
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
         <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
          <div class="item">
            <div class="testimonial">
              <p>Although a couple of oganisations came to distribute relief material, we were not able to access it as we were left behind due to old age. CARE India identified us and came to our doorstep to distribure relief kits. </p>
            </div>
            <div class="test-footer"> <strong>Salina Chhetri</strong> <span>Pokhara</span> </div>
          </div>
        </div>
      </div>
    </div>
  <!--Testimonial-->
<!--footer -->
<?php include('footer.php');?>

</body>
</html>