 <?php include('header.php');
  
  ?> 
  
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>News and Events</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">News</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
    <div class="row">
       <?php
                $latPhotos=$mysqli->query("SELECT * FROM activites WHERE acid=7");
                  while($SiPhotos=$latPhotos->fetch_array()){
                 $aboutid=$SiPhotos["acid"];
                $title=$SiPhotos["TITLE"];
                $description=$SiPhotos["Description"];
                $image=$SiPhotos["image"];
                $date=$SiPhotos["dates"];
                ?>
      <div class="col-lg-9">
        <div class="news-list news-detail">
              
                <div class="news-content no-padding">
                  <h3 class="title title1"><a href="#"><?=$title?></a></h3>
                    <small><?=$date?></small>
                    <div class="image-wrapper"></div>
                    <p><?=$description?></p>
                    <ul class="list">
                      <li> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</li>
                        <li>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet</li>
                        <li>Cum sociis natoque penatibus et magnis dis parturient montes</li>
                    </ul>
                    
                </div><?php }?>
          </div>
       
      </div>
      <div class="col-lg-3 sidebar">
         <div class="block">
    <h4>News & Events</h4>
    <div class="newslist">
      <ul>
        <li><a href="#"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
        <li><a href="#"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
        <li><a href="#"> <strong class="">Lorem ipsum dolor sit amet</strong></a> <small>January 25, 2018</small>
          <p>Sed ut perspiciatis unde omnis iste natus</p>
        </li>
      </ul>
    </div>
  </div>
      </div>
    </div>
  </div>
  </div>

 <?php include('footer.php');?> 