<?php 
require_once("AdminLTE/inc/config.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Child and Women Empowerment Society Nepal</title>
<!--google fonts-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/animations.css" rel="stylesheet">
<!--Icon Fonts-->
<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- Plugins -->
<link href="plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
<link href="css/animations.css" rel="stylesheet">
<link href="css/scrolleffect.css" rel="stylesheet">
<link rel="stylesheet" href="css/owl.carousel.min.css">

<!--Core Css-->
<link href="css/inline.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">


</head>

<body>
<!-- scrollToTop --> 
<!-- ================ -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<div class="page-wrapper">

<!-- header start --> 
<!-- ================ -->

<header class="clearfix" data-spy="affix" data-offset-top="60">
  <!-- <div class="container custom-container"> -->
  <div id="logo" class="hidden-xs"> <a href="index.html"><img src="img/logo.png" alt=""></a> </div>
  <div class="header-right">
    <div class="header-right-top clearfix hidden-xs hidden-sm">
      <!-- <ul class="header-contact pull-left">
       sdgsdgs
        
      </ul> -->
      <ul class="tp-nav-social pull-right">
        <li>
          <div class="mid-header text-right">
             <a class="btn donate-btn" href="donate.html"><i class="icon donate-icon"><img src="img/66185.png" alt="donate"></i>Donate Here</a> <a class="btn volunteer-btn" href="volunteer.html"><i class="icon volunteer-icon"><img src="img/4060-200.png" alt="volunteer"></i>Volunteer</a>
          </div>
        </li>
        <li class="Social"><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li class="Social"><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li class="Social"><a href="#"><i class="fa fa-instagram"></i></a></li>
      </ul>
    </div>
    <div class="navigation">
      
         
      <nav class="navbar navbar-default navbar-right"> 
        
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt="logo"></a> </div>
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
           
            <li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us<span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu">
                <?php 
                  $menu =$mysqli->query("SELECT *  FROM about");
                   while( $query=$menu->fetch_array()){
                          $ActivityId=$query["Aboutid"];
                          $Title=$query["Title"];
                    
                ?>
               <li><a href="aboutdetail.php?id=<?=$ActivityId?>" class="dropdown"><?=$Title?></a></li>
                
                <?php }?>
              </ul>
              </li>
              
            
           
         <li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects<span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu">
                 <li><a href="project.php">Recent Project</a></li>
                  
                </ul>
              </li>
            
        
            <li class="dropdown"><a href="about.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Volunteers<span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu">
                  <li><a href="volunteer.php">Join form Volunteer</a></li>
          
                </ul>
              </li>
              
              </li>
            <li><a href="gallery.php">Gallery</a></li>
            <li><a href="testimonial.php">Testimonial</a></li>
            <li><a href="contact.php">Contact Us</a></li>
         
        <!-- /.navbar-collapse --> 
        
      </nav>
    </div>
  </div>
<!-- </div> -->
</header>