<?php
require_once("AdminLTE/inc/config.php");

    $latPackage=$mysqli->query("select * from about where Aboutid=8");
    $SiPackage=$latPackage->fetch_array();
    $Activity=$SiPackage["Aboutid"];
    $Title=$SiPackage["Title"];
    $Description=$SiPackage["description"];
    $objective=$SiPackage['objective'];
    $Photo=$SiPackage["image"];
?>
<!-- header-->   
<?php include('header.php'); ?>
  <div class="page-top parallax dark-translucent margin30">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
           
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">About Us</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="img/<?=$Photo?>">
      </div>
      <div class="col-lg-6">
        
        <ul class="list sec-title">
          <h1>Our Goals </h1>
          <span class="line"></span>
                  <li><?=$Description?>
                  </li>
                  <h1>Objectives</h1>
                  <span class="line"></span>
                  <li><?=$objective?>
                  </li>
                  
                  
                </ul>
      </div>
    </div>
  </div>

  <!--footer -->
<?php include('footer.php');?>
</body>
</html>