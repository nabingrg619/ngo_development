<?php    
require('AdminLTE/inc/config.php');
if(isset($_POST['submit']))
{
  $Nickname = $_POST['txtNickname'];
  $Address=$_POST["txtAddress"];
  $Review= $_POST["txtReview"];
  $add_sql = $mysqli->query("INSERT INTO reviews SET Nickname='$Nickname',address= '$Address',Review = '$Review',IsPublished=0");
    if($add_sql = TRUE)
    {
      $successMsg = '<div class="alert alert-success">Successfully review Added</div>';
      echo "<meta http-equiv='refresh' content='0'>";
      echo "<script>alert('Thanks for your review.It will be published once reviewed.');
            window.location.href='index.php';
            </script>";
    }
    else
    {
      $successMsg = '<div class="alert alert-success">Some Error!!! Contact to Web Page Nepal for IT Help.</div>';
    }
}
?>

<!-- header-->  
 <?php include('header.php'); ?>
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Testimonial</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Testimonial</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="inner-page padding40 clearfix">
      <ul class="testimonial-wrap">
        <?php
        $latReviews=$mysqli->query("select * from reviews");
          while($SiReviews=$latReviews->fetch_array()){
                    $Nickname=$SiReviews["Nickname"];
                    $Review=$SiReviews["Review"];
                    
                    $Date=$SiReviews["Date"];
          
                ?>
                <li> <span class="avtar"><i class="fa fa-user"></i></span>
          <div class="testimonials"> 
            <span class="name"><?=$Nickname?></span> 
            <span class="date"><?=$Date?></span>
            <p> <?=$Review?></p>
          </div>
        </li>
       <?php } ?>
           </ul>
      <div class="row">
        <div class="col-md-12">
          <div class="review-form clearfix">
            <h2 class="title">Write your own review</h2>
            <form method="post" >
              <div class="form-group">
                <label>Full Name<sup>*</sup></label>
                <input name="txtNickname" id="txtNickname" class="form-control" type="text">
              </div>
              <div class="form-group">
                <label>Address<sup>*</sup></label>
                <input name="txtAddress" id="txtNickname" class="form-control" type="text">
              </div>
               <div class="form-group">
                <label>Your Review</label>
                <textarea class="form-control" name="txtReview" id="txtReview" rows="3"></textarea>
              </div>
              <div class="form-group">
                <!-- <button type="submit" name="" class="btn btn-default">Submit</button> -->
                <button type="submit" name="submit" class="btn btn-outline">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>

<!--footer -->
 <?php include('footer.php');?>
</body>
</html>