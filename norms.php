<?php
require_once("AdminLTE/inc/config.php");
    $latPackage=$mysqli->query("select * from about where Aboutid=5");
    $SiPackage=$latPackage->fetch_array();
    $Activity=$SiPackage["Aboutid"];
    $Title=$SiPackage["Title"];
    $Description=$SiPackage["description"];
    $Photo=$SiPackage["image"];
?>
<!--header--> 
<?php include('header.php'); ?>
  <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Norms and Values</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Norms</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="img/<?=$Photo?>">
      </div>
      <div class="col-lg-6">
        
        <ul class="list sec-title">
          <h1><?=$Title?></h1>
          <span class="line"></span>
                  <li><?=$Description?></li>
                  
                  
                </ul>
      </div>
    </div>
  </div>
  </div>
<!--footer -->
 <?php include('footer.php');?>
</body>
</html>