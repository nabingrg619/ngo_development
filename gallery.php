<?php
require_once("AdminLTE/inc/config.php");
?>
<?php include('header.php'); ?>
 
 
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Gallery</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Gallery</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
 <section class="innerpage about-content">
  <div class="container">
    <div class="masonry-grid row"> 
      <?php
      $latPhotos=$mysqli->query("select * from gallery");
        while($SiPhotos=$latPhotos->fetch_array()){
        $Photo=$SiPhotos["image"];
      ?>
       <!-- masonry grid item start -->
          <div class="masonry-grid-item col-sm-6 col-md-4">
            <!-- blogpost start -->
              <article class="clearfix blogpost">
                <div class="overlay-container">
                  <img src="img/<?=$Photo?>" alt="">
                      <div class="overlay">
                        <div class="overlay-links">
                          <a href="img/<?=$Photo?>" class="popup-img-single" title="image title"><i class="fa fa-search-plus"></i></a>
                        </div>
                          </div>
                            </div>
                          </article>
  <!-- blogpost end --> 
</div>
  <?php }?>
    </div>
      </div>
        </div> 
      </section>
<!--footer -->
<?php include('footer.php');?>
</body>
</html>