
<?php
require_once("inc/config.php");
$successMsg = "";
$scr = "";
$strDate = strtotime(date('Y-m-d H:i:s'));
if(isset($_POST['newsSubmit'])){
    $newsTitle = ($_POST['title']);
    $newsDes = $_POST['des'];
    $context = $_POST['context'];
    $themes = $_POST['themes'];
    $sub = $_POST['sub'];
    $donor = $_POST['donor'];
    $area = $_POST['area'];
    $duration= $_POST['dur'];
    $major=$_POST['major'];
    $include=$_POST['inc'];
    $hiv=$_POST['hiv'];
    $peer=$_POST['peer'];
    $strength=$_POST['str'];
    $community=$_POST['comm'];
    define ("MAX_SIZE","55000");
 function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }

 
 $errors=0;
   
 if($_SERVER["REQUEST_METHOD"] == "POST")
 {
    $image =$_FILES["file"]["name"];
    $uploadedfile = $_FILES['file']['tmp_name'];
      
  
    if ($image) 
    {
     
        $filename = stripslashes($_FILES['file']['name']);
     
        $extension = getExtension($filename);
        $extension = strtolower($extension);
         
         
 if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
        {
         
            $change='<div class="msgdiv">Unknown Image extension </div> ';
            $errors=1;
        }
        else
        {
 
 $size=filesize($_FILES['file']['tmp_name']);
 
 
if ($size > MAX_SIZE*5024)
{
    $change='<div class="msgdiv">You have exceeded the size limit!</div> ';
    $errors=1;
}
 
 
if($extension=="jpg" || $extension=="jpeg" )
{
$uploadedfile = $_FILES['file']['tmp_name'];
$src = imagecreatefromjpeg($uploadedfile);
 
}
else if($extension=="png")
{
$uploadedfile = $_FILES['file']['tmp_name'];
$src = imagecreatefrompng($uploadedfile);
 
}
else
{
$src = imagecreatefromgif($uploadedfile);
}
 
echo $scr;
 
list($width,$height)=getimagesize($uploadedfile);
 
 
$newwidth=120;
$newheight=94;
 
$newwidth1=$width;
$newheight1=$height; 
 
$tmp=imagecreatetruecolor($newwidth,$newheight);
$tmp1=imagecreatetruecolor($newwidth1,$newheight1);
 
imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
 
imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height); 
 
$filename = "../img/".$strDate.strtolower(($_FILES['file']['name']));
 
$filename1 = "../img/".$strDate.strtolower(($_FILES['file']['name']));
$filename16 =$strDate.strtolower(($_FILES['file']['name'])); 
 
imagejpeg($tmp,$filename,100);
 
imagejpeg($tmp1,$filename1,100);
 
imagedestroy($src);
imagedestroy($tmp);
imagedestroy($tmp1);
}}
 }

    $add_sql = $mysqli->query("INSERT INTO projects SET title = '$newsTitle', description = '$newsDes', image = '$filename16',context='$context',themes='$themes',sub_title='$sub',donor='$donor',area='$area',duration='$duration',major='$major',include='$include',hiv='$hiv',peer='$peer',strength='$strength',community='$community'");
    if($add_sql = TRUE){
      $successMsg = '<div class="alert alert-success">Successfully Added</div>' or die();
    }else{
      $successMsg = '<div class="alert alert-success">Some Error!!! Contact to Web Page Nepal for IT Help.</div>';
    } 
}
?>

<?php include('inc/head.php');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php
    #top nav, aside menu
        require_once("inc/header.php");
    ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        News
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">About</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        <?=$successMsg?>
          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Add NEWS</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              <form action="" method="post" enctype="multipart/form-data">
                  <label>title</label>
                <div class="form-group">
                  <input type="text" class="form-control" name="title" placeholder="Title">
                </div>
                <label>sub-titletitle</label>
                <div class="form-group">
                  <input type="text" class="form-control" name="sub" placeholder="Sub-Title">
                </div>
                <label>donor</label>
                <div>
                  <textarea name="donor" class="textarea" placeholder="Donor"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <label>area</label>
                <div>
                  <textarea name="area" class="textarea" placeholder="area"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <label>duartion</label>
                <div>
                  <textarea name="dur" class="textarea" placeholder="Duration"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <label>description</label>
                <div>
                  <textarea name="des" class="textarea" placeholder="Description"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <label>context</label>
               <div>
                  <textarea name="context" class="textarea" placeholder="Context"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <label>themes</label>
                <div>
                  <textarea name="themes" class="textarea" placeholder="Themes"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                   <label>Major activites</label>
                <div>
                  <textarea name="major" class="textarea" placeholder="major activities"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                   <div>
                  <textarea name="inc" class="textarea" placeholder="major activities"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                   <div>
                  <textarea name="hiv" class="textarea" placeholder="major activities"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                   <div>
                  <textarea name="peer" class="textarea" placeholder="major activities"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                   <div>
                  <textarea name="str" class="textarea" placeholder="major activities"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                   <div>
                  <textarea name="comm" class="textarea" placeholder="major activities"
                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                <label>image</label>
                <div>
                    <input name="file" type="file" />
                </div>
                  
            <div class="box-footer clearfix">
              <button type="submit" name="newsSubmit" class="pull-right btn btn-default" id="sendEmail">Save
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
              </form>
            </div>
          </div>
  </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<?php include('inc/foot.php');?>


