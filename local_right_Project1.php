<?php include('header.php');?>
 

<!-- header end -->  
 <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Recent Project</h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Recent Project</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
    <div class="container">
      <div class="row">
        <?php
                $latPhotos=$mysqli->query("SELECT * FROM projects WHERE proid=7");
                  while($SiPhotos=$latPhotos->fetch_array()){
                $aboutid=$SiPhotos["proid"];
                $title=$SiPhotos["title"];
                $description=$SiPhotos["description"];
                $context=$SiPhotos["context"];
                $themes=$SiPhotos["themes"];

                
                ?>
        <div class="list sec-title">
          <div class="col-lg-6">
          <h1 class=""><?=$title?></h1>
            <span class="line"></span>
        </div>
        <div class="col-lg-6 text-right">
          <a href="project.php" class="breadcrumb">
         <i class="fa fa-tasks" aria-hidden="true"></i>
Back to Project
        </a>
        </div>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-12">
       
         <ul class="list sec-title">
          <p class="details"> 
           <?=$context?>
          </p>

                  
                 <h1 class="pad10">Context:</h1>
                  <p class="details"> 
                     <?=$themes?>
                  </p>
                  <h4 class="margin3">  Themes</h4>
                  <ul class="theme">  
                    <li> <?=$description?> </li>
                    
                  </ul>
                </ul><?php }?>
      </div>
      
      <div class="col-lg-6">
        
       
      </div>
    </div>
  </div>
  </div>

    <!--footer -->
 <?php include('footer.php');?>

</body>
</html> 