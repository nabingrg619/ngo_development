<?php 
      require('AdminLTE/inc/config.php');
      $ActivityId=$_GET['id'];
      $latPackage=$mysqli->query("SELECT Aboutid,Title,description,image FROM about WHERE Aboutid=$ActivityId");
      $SiPackage=$latPackage->fetch_array();
      $Activity=$SiPackage["Aboutid"];
      $Title=$SiPackage["Title"];
      $Description=$SiPackage["description"];
      $Photo=$SiPackage["image"];
?>
<!-- header-->  
 <?php include('header.php'); ?>
  <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2><?=$Activity,?></h2>
            <span class="seperator_inner"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="#">Norms</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="margin30">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
         <img src="img/<?=$Photo?>">
      </div>
      <div class="col-lg-6">
        <ul class="list sec-title">
           <h1> <?=$Title?></h1> 
          <span class="line"></span>
                <p>
               <?=$Description?>
                </p>
         <!--  <h1>Mission</h1> 
          <span class="line"></span>
                <p>
                 Ensure social rights of marginalized women and children with promotion of their education, health and economic growth

                </p>
                <h1>Vision</h1> 
          <span class="line"></span>
                <p>Creation of gender balanced society with social security</p> -->
        </ul>
      
      </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-sm-6">
           <div class="mission">
                <div class="text-center">
                  <div class="block-number">01</div>
                        <h2><a href="#">Our Mission</a></h2>
                        <div class="title">Ensure social rights of marginalized women and children with promotion of their education, health and economic growth</div>
                  </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6">
           <div class="mission">
                <div class="text-center">
                  <div class="block-number">02</div>
                      <h2><a href="#">Our Vision</a></h2>
                          <div class="title">Creation of gender balanced society with social security</div>
                </div>
            </div>
        </div>
    </div>
  </div>

<!--footer -->
<?php include('footer.php');?>
</body>
</html>