<?php 
require_once("inc/config.php");
?>
<?php include('inc/head.php');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php
    #top nav, aside menu
        require_once("inc/header.php");
    ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        PROJECTS
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Projects</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>
			  <h3 class="box-title">EDIT PROJECTS</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
			<ul id="ulNewsList">
            <?php
            $latNews = $mysqli->query("SELECT * FROM projects ORDER BY proid DESC");
            while($fth_latNews = $latNews->fetch_array()){
                $ActivityId = $fth_latNews['proid'];
                $Title = $fth_latNews['title'];
            ?>
	<li>
          <a href="EditprojectsDetail.php?id=<?=$ActivityId?>"><?=$Title?></a> 
	         <a href="Deleteproject.php?id=<?=$ActivityId?>">
             <span data-toggle="tooltip" title="Delete Activity" id="<?=$ActivityId?>" class="glyphicon glyphicon-trash delActivity"></span>
           </a>
	</li>
		   <?php
            } ?>
			</ul>
            </div>
          </div>

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
       
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include('inc/foot.php');?>